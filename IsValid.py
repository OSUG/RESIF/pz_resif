#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import yaml
import jsonschema
import jsonref
import os

PZ_FORMAT = ['response','filter']

def load_file(filename):
    try:
        element = yaml.safe_load(open(filename,'r'))
    except:
        print(f"Error loading YAML file: {filename}")
        return
    return element


def read_schema(pz_format):

    schema_path = f'schema/{pz_format}.schema.json'
    
    with open(schema_path,'r') as f:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        base_path=os.path.dirname(f'{current_dir}/schema/')
        base_uri=f'file://{current_dir}/schema/' 
        schema = jsonref.loads(f.read(),base_uri=base_uri,jsonschema=True)

    return schema

def validate(filename):
    """
        Validates YAML file against schema

        return True if the filename is valide
        else list of errors 
    """
    # read information
    instance = load_file(filename)

    pz_type = get_type(filename)

    schema = read_schema(pz_type)

    #test if file is valid
    valid_schema = jsonschema.Draft4Validator(schema)
    if valid_schema.is_valid(instance):
        return True

    # get errors messages    
    errors = []
    for error in valid_schema.iter_errors(instance):
        error_element = "".join(f"[{err}]" for err in error.path)
        errors.append(f"{error_element}: {error.message}")
    return errors 


def get_type(filename):
    """
      Get type from filename. 
      The format of filename is "...TYPE.YAML"
    """
    file_type = filename.split('/')[-1].split('.')[-2]

    if file_type not in PZ_FORMAT:
        print(f" Unknown type pz: {file_type}. Please verfie your the format of filename. It must be *.TYPE.YAML. The type can be response or filter")
        exit(-1)
    return file_type

def main(filename):

    isValid = validate(filename)
    if type(isValid) is bool:
        print (f" The file {filename} is valid ")
    else:
        for i in isValid:
            print("\t",i)


if __name__ == '__main__':
    
    from argparse import ArgumentParser

    parser = ArgumentParser( prog='validate PZ', description=__doc__)
    group = parser.add_argument_group()
    group.add_argument("-f","--file", help='Path to filename')
    group.add_argument("-d","--dir", help='Path to dir')

    args = parser.parse_args()
    if args.file:
        main(args.file)
    else:
        for filename in os.listdir(args.dir):
            if os.path.isdir(f'{args.dir}/{filename}') :
                print(f" {filename} is not file")
            else:
                main(f'{args.directory}/{filename}')

    